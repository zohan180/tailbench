#include "tbench_client.h"
#include <string>
#include <string.h>
/*******************************************************************************
 * API 
 *******************************************************************************/
void tBenchClientInit() {
    // Client::init();
}

// this is called in the /harness/client.cpp getting request
size_t tBenchClientGenReq(void *data) {
    std::string str = "Dummy Req";
    size_t len = str.size() + 1;
    memcpy(data, reinterpret_cast<const void*>(str.c_str()), len);

    return len;
    // return Client::getSingleton()->get(data);
}
