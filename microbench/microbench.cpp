#include "tbench_server.h"
#include "helpers.h"
#include <unistd.h>
#include <math.h>

#include <atomic>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <pthread.h>
#include <random>

#include <sstream>

using namespace std;

void printHelp(char* argv[]) {
    cerr << endl;
    cerr << "Usage: " << argv[0] << " [-f model_file] [-n max_reqs]" \
        << " [-r threads] [-h]" << endl << endl;
    cerr << "-f : Name of model file to load " << "(default: model.xml)" \
        << endl; 
    cerr << "-n : Maximum number of requests "\
        << "(default: 6000; size of the full MNIST test dataset)" << endl;
    cerr << "-r : Number of worker threads" << endl;
    cerr << "-h : Print this help and exit" << endl;
}

static inline uint64_t GetCurrentClockCycle() {
#if defined(__x86_64__) || defined(__amd64__)
  uint64_t high, low;
  __asm__ volatile("rdtsc" : "=a"(low), "=d"(high));
  return (high << 32) | low;
  // ----------------------------------------------------------------
#elif defined(__aarch64__)
  // System timer of ARMv8 runs at a different frequency than the CPU's.
  // The frequency is fixed, typically in the range 1-50MHz.  It can because
  // read at CNTFRQ special register.  We assume the OS has set up
  // the virtual timer properly.
  uint64_t virtual_timer_value;
  asm volatile("mrs %0, cntvct_el0" : "=r"(virtual_timer_value));
  return virtual_timer_value;
#else
  return DUMMY_CYCLE_CLOCK;
#endif
}

static inline uint64_t GetFrequency() {
#if defined(__x86_64__) || defined(__amd64__)
    // this number is based on the system running now.
    // maybe a better to get this number from compile environment
  uint64_t rdt_freq = 3693200000;
    return rdt_freq;
#elif defined(__aarch64__)
  // System timer of ARMv8 runs at a different frequency than the CPU's.
  // The frequency is fixed, typically in the range 1-50MHz.  It can because
  // read at CNTFRQ special register.  We assume the OS has set up
  // the virtual timer properly.
  uint64_t virtual_timer_freq;
  asm volatile("mrs %0, cntfrq_el0" : "=r"(virtual_timer_freq));
  return virtual_timer_freq;
#else
  return DUMMY_FREQ;
#endif
}

pthread_mutex_t microLock1, microLock2;
// bool underNoise[10] = {false};
// int sCnt, nCnt, bCnt, nbCnt;
// std::exponential_distribution<double> eDist(1 * 1e-9);
//std::uniform_int_distribution<int> nDist{0,1}; 
std::random_device rd;
std::default_random_engine random_g(rd());

class Worker {
private:
    int tid;
    pthread_t thread;
    
    long nReqs;
    static atomic_llong nReqsTotal;
    static long maxReqs;
    static atomic_llong correct;
    
    double spinLimit;
    double block1T, block1V, block2T, block2V, noiseT;

    double dist1T, dist2T, dist3T, dist4T;
    double dist1V, dist2V, dist3V, dist4V;
    int dist2P, dist3P, dist4P, noiseP;

    std::uniform_int_distribution<int> uDist;

    
    static void* run(void* ptr) {
        Worker* worker = reinterpret_cast<Worker*>(ptr);
        worker->doRun();
        
        return nullptr;
    }
  
    void doRun() {
        tBenchServerThreadStart();
        printf("tid is: %i\n",tid);
        char* request;
        
        //get system clock frequency, should be 54MHz on rasp pi
        uint64_t virt_freq = GetFrequency();

        // int lockIdx;
        double dist1Ave = virt_freq*dist1T/1e+6;
        double dist2Ave = virt_freq*dist2T/1e+6;
        double dist3Ave = virt_freq*dist3T/1e+6;
        double dist4Ave = virt_freq*dist4T/1e+6;

        double block1Ave = virt_freq*block1T/1e+6;
        double block2Ave = virt_freq*block2T/1e+6;

        double noiseAve = virt_freq*noiseT/1e+6;
        double spinCycle = virt_freq*spinLimit/1e+6;

        // std::normal_distribution<double> nBlockDist(nBlockAve,virt_freq*(nBlockT/nBlockVar)/1e+6);
        // std::normal_distribution<double> noiseLengthDist(noiseAve,noiseAve/noiseVar);
        // std::normal_distribution<double> sleepLengthDist(sleepT,sleepT/100);

        std::lognormal_distribution<double> dist1TDist(std::log(dist1Ave),dist1V);
        std::lognormal_distribution<double> dist2TDist(std::log(dist2Ave),dist2V);
        // std::uniform_real_distribution<double> dist2TDist(virt_freq*300/1e+6,virt_freq*720/1e+6);
        std::lognormal_distribution<double> dist3TDist(std::log(dist3Ave),dist3V);
        std::lognormal_distribution<double> dist4TDist(std::log(dist4Ave),dist4V);

        std::lognormal_distribution<double> block1Dist(std::log(block1Ave),block1V);
        std::lognormal_distribution<double> block2Dist(std::log(block2Ave),block2V);

        std::uniform_int_distribution<int> noisePDist(0, 999999);
        std::uniform_int_distribution<int> distPDist(0, 999999);

        if (tid==0){
            printf("base freq: %lu. \n", virt_freq);
            printf("dist1 mean time: %.3f, %.3f cycles, log: %.3f, var: %.3f.\n", dist1T, dist1Ave, std::log(dist1Ave), dist1V);
            printf("dist2 mean time: %.3f, %.3f cycles, log: %.3f, var: %.3f, with prob: %i.\n", dist2T, dist2Ave, std::log(dist2Ave), dist2V, dist2P);
            printf("dist3 mean time: %.3f, %.3f cycles, log: %.3f, var: %.3f, with prob: %i.\n", dist3T, dist3Ave, std::log(dist3Ave), dist3V, dist3P);
            printf("dist4 mean time: %.3f, %.3f cycles, log: %.3f, var: %.3f, with prob: %i.\n", dist4T, dist4Ave, std::log(dist4Ave), dist4V, dist4P);
            printf("block1 mean time: %.3f, %.3f cycles, log: %.3f, var: %.3f.\n", block1T, block1Ave, std::log(block1Ave), block1V);
            printf("block2 mean time: %.3f, %.3f cycles, log: %.3f, var: %.3f.\n", block2T, block2Ave, std::log(block2Ave), block2V);
            printf("noise mean time: %.3f, %.3f cycles, prob: %i. \n", noiseT, noiseAve, noiseP);
            printf("spin limit:%.3f, cycles: %lu.\n", spinLimit, spinCycle);
        }

        // long long block1Cycle, block2Cycle, noiseCycle, distCycle;
        long long noiseCycle, distCycle, block1Cycle, block2Cycle;;
        long long t_start, t_end;

        noiseCycle = noiseAve;
        // block1Cycle = block1Ave;
        // block2Cycle = block2Ave;

        while (true) { 
            // printf("new one start.");
            ++nReqs;
            size_t len = tBenchRecvReq(reinterpret_cast<void**>(&request));
            uint64_t cnt; 
            int distNum = distPDist(rd);
            // printf("nblockCycle done: %lld.",nBlockCycle);
            int bcnt = 0;

            hybridLock(&microLock1,spinCycle);
            //pthread_mutex_lock(&microLock1);
            if (block1Ave){
                block1Cycle = std::llround(block1Dist(random_g));
                t_start=GetCurrentClockCycle();
                t_end=GetCurrentClockCycle();
                while(!((t_end-t_start)/block1Cycle)){
                    t_end=GetCurrentClockCycle();
                }
                // printf("block1Cycle done: %lld.\n",block1Cycle);
            }
            pthread_mutex_unlock(&microLock1);
            //randomized change to add noise disturbance

            if(distNum < dist4P){
                distCycle = std::llround(dist4TDist(random_g));
            } else if(distNum < (dist4P + dist3P) ){
                distCycle = std::llround(dist3TDist(random_g));
            } else if(distNum < (dist4P + dist3P + dist2P) ){
                distCycle = std::llround(dist2TDist(random_g));
            } else {
                distCycle = std::llround(dist1TDist(random_g));
            }
            
            t_start=GetCurrentClockCycle();
            t_end=GetCurrentClockCycle();
            while(!((t_end-t_start)/distCycle)){
                t_end=GetCurrentClockCycle();
                // printf("%u, %u \n",t_start,t_end);
            }

            if(noisePDist(rd) < noiseP){

                t_start=GetCurrentClockCycle();
                t_end=GetCurrentClockCycle();
                while(!((t_end-t_start)/noiseCycle)){
                    t_end=GetCurrentClockCycle();
                    // printf("%u, %u \n",t_start,t_end);
                }
                // nCnt++;
            }
            // printf("noise done: %u;",noiseCycle);
            hybridLock(&microLock2,spinCycle);
            //pthread_mutex_lock(&microLock2);
            if (block2Ave){
                block2Cycle = std::llround(block2Dist(random_g));
                t_start=GetCurrentClockCycle();
                t_end=GetCurrentClockCycle();
                while(!((t_end-t_start)/block2Cycle)){
                    t_end=GetCurrentClockCycle();
                }
            }
            pthread_mutex_unlock(&microLock2);

            asm volatile("nop");
            cnt = cnt*1e+9/virt_freq;
            tBenchSendResp(&cnt, sizeof(cnt));
        }
    }
  
public:
  Worker(int tid, 
            double dist1T, double dist1V, \
            double dist2T, double dist2V, int dist2P, \
            double dist3T, double dist3V, int dist3P, \
            double dist4T, double dist4V, int dist4P, \
            double block1T, double block1V, \
            double block2T, double block2V, \
            double noiseT, double noiseP, \
            double spinLimit)
    : tid(tid) 
    , nReqs(0)
    // , spinLimit(spinMax)
    , dist1T(dist1T), dist1V(dist1V)
    , dist2T(dist2T), dist2V(dist2V), dist2P(dist2P)
    , dist3T(dist3T), dist3V(dist3V), dist3P(dist3P)
    , dist4T(dist4T), dist4V(dist4V), dist4P(dist4P)
    , block1T(block1T), block1V(block1V)
    , block2T(block2T), block2V(block2V)
    , noiseT(noiseT), noiseP(noiseP)
    , spinLimit(spinLimit)
    // , timeFreeze(timeFreeze)
  { }
  
  void run() {
    pthread_create(&thread, nullptr, Worker::run, reinterpret_cast<void*>(this));
  }
  
  void join() {
    pthread_join(thread, nullptr);
  }
  
  static long correctDecodes() { return correct; }
  
  // static void updateMaxReqs(long _maxReqs) { maxReqs = _maxReqs; }
  
};

atomic_llong Worker::nReqsTotal(0);
long Worker::maxReqs(0);
atomic_llong Worker::correct(0);

int 
main(int argc, char** argv)
{
    int nThreads = 1;
    std::string config_file = "";
    pthread_mutex_init(&microLock1, nullptr);
    pthread_mutex_init(&microLock2, nullptr);
    int c;
    int low,high = 0;
    while ((c = getopt(argc, argv, "r:f:h")) != -1) {
        switch(c) {
        case 'r':
        nThreads = atoi(optarg);
        break;
        case 'f':
        config_file = optarg;
        break;
        case 'h':
        printHelp(argv);
        return 0;
        break;
        case '?':
        printHelp(argv);
        return -1;
        break;
        }
    }

    // double dist1T = getOpt<double>("DIST1_TIME", 10.00);
    // double dist1V = getOpt<double>("DIST1_VAR", 1.00);

    // double dist2T = getOpt<double>("DIST2_TIME", 1000.00);
    // double dist2V = getOpt<double>("DIST2_VAR", 1.00);
    // int dist2P    = getOpt<int>("DIST2_PROB", 0);

    // double dist3T = getOpt<double>("DIST3_TIME", 1000.00);
    // double dist3V = getOpt<double>("DIST3_VAR", 1.00);
    // int dist3P    = getOpt<int>("DIST3_PROB", 0);

    // double block1T = getOpt<double>("BLOCK1_TIME", 0.00);
    // double block1V = getOpt<double>("BLOCK1_VAR", 1.00);
    // double block2T = getOpt<double>("BLOCK2_TIME", 0.00);
    // double block2V = getOpt<double>("BLOCK2_VAR", 1.00);
    
    // double noiseT = getOpt<double>("NOISE_TIME", 0.00);
    // int noiseP    = getOpt<int>("NOISE_PROB", 1000000);

    // double spinLimit    = getOpt<double>("SPIN_LIMIT", 1);

    // double spinLimit = 1;
    // int noiseP = 1000000;

    vector<double> configs;

    // open file    
    ifstream inputFile(config_file);
    // std::cout<<"Read: inputFile"<<std::endl;
    // test file open   
    if (inputFile) {
        // std::cout<<"Read: inputFile"<<std::endl;
        double value;
        // read the elements in the file into a vector  
        while ( inputFile >> value ) {
            configs.push_back(value);
            // std::cout<<"Read: "<<value<<std::endl;
        }
    }

    for (auto any : configs){
        // std::cout<<"Read: "<<any<<std::endl;
    }

    double dist1T = configs[0];
    double dist1V = configs[1];

    double dist2T = configs[2];
    double dist2V = configs[3];
    int    dist2P = int(configs[4]);

    double dist3T = configs[5];
    double dist3V = configs[6];
    int    dist3P = int(configs[7]);

    double dist4T = configs[8];
    double dist4V = configs[9];
    int    dist4P = int(configs[10]);

    double block1T = configs[11];
    double block1V = configs[12];
    double block2T = configs[13];
    double block2V = configs[14];

    double noiseT  = configs[15];
    double noiseP  = configs[16];
    
    double spinLimit = configs[17];
// close the file

    tBenchServerInit(nThreads);
    // Worker::updateMaxReqs(maxReqs);
    vector<Worker> workers;
    for (int t = 0; t < nThreads; ++t) {
        workers.push_back(Worker(t, \
            dist1T, dist1V, \
            dist2T, dist2V, dist2P, \
            dist3T, dist3V, dist3P, \
            dist4T, dist4V, dist4P, \
            block1T, block1V, \
            block2T, block2V, \
            noiseT, noiseP, \
            spinLimit));
    }
    
    for (int t = 0; t < nThreads; ++t) {
        workers[t].run();
    }
    
    for (int t = 0; t < nThreads; ++t) {
        workers[t].join();
    }
    
    return 0;
}
