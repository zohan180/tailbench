/** $lic$
 * Copyright (C) 2016-2017 by Massachusetts Institute of Technology
 *
 * This file is part of TailBench.
 *
 * If you use this software in your research, we request that you reference the
 * TaiBench paper ("TailBench: A Benchmark Suite and Evaluation Methodology for
 * Latency-Critical Applications", Kasture and Sanchez, IISWC-2016) as the
 * source in any publications that use this software, and that you send us a
 * citation of your work.
 *
 * TailBench is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 */

#ifndef __HELPERS_H
#define __HELPERS_H

#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <arpa/inet.h>
#include <netdb.h>
template<typename T>
static T getOpt(const char* name, T defVal) {
    const char* opt = getenv(name);

    if (!opt){
        std::cout << name << " = " << defVal << std::endl;
        return defVal;
    }
    else{
        std::cout << name << " = " << opt << std::endl;
    }
    std::stringstream ss(opt);
    if (ss.str().length() == 0) return defVal;
    T res;
    ss >> res;
    if (ss.fail()) {
        std::cerr << "WARNING: Option " << name << "(" << opt << ") could not"\
            << " be parsed, using default" << std::endl;
        return defVal;
    }   
    return res;
}

static uint64_t getCurNs() {
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    uint64_t t = ts.tv_sec*1000*1000*1000 + ts.tv_nsec;

    // uint64_t t1 = ts.tv_sec*1000*1000*1000;
    // uint64_t t2 = ts.tv_nsec;
    // uint64_t t  = t1 + t2;
    
    // std::cout <<ts.tv_sec<<" become: "<< t1 <<"+"<<t2<<", getCurNs calculated:" << t << std::\
endl;


    return t;
}

static void sleepUntil(uint64_t targetNs) {
    uint64_t curNs = getCurNs();
    while (curNs < targetNs) {
        uint64_t diffNs = targetNs - curNs;
        struct timespec ts = {(time_t)(diffNs/(1000*1000*1000)), 
            (time_t)(diffNs % (1000*1000*1000))};
        nanosleep(&ts, NULL); //not guaranteed, hence the loop
        curNs = getCurNs();
    }
}

static int sendfull(int fd, const char* msg, int len, int flags) {
    int remaining = len;
    const char* cur = msg;
    int sent;
    //std::cout<<"send";
    while (remaining > 0) {
      //std::cout<<"1";
        sent = send(fd, reinterpret_cast<const void*>(cur), remaining, flags);
        if (sent == -1) {
	  std::cerr << "send() failed: " << strerror(errno)<<",with len:"<< len << ",left:"<<remaining <<std::endl;
            break;
        }
        cur += sent;
        remaining -= sent;
    }
    //std::cout<<"\n";
    return (len - remaining);
}

static int recvfull(int fd, char* msg, int len, int flags) {
    int remaining = len;
    char* cur = msg;
    int recvd;

    //std::cout << "here in helpers.h: recvfull" << std::endl;
    while (remaining > 0) {
        recvd = recv(fd, reinterpret_cast<void*>(cur), remaining, flags);
        if ((recvd == -1) || (recvd == 0)) break;
        cur += recvd;
        remaining -= recvd;
    }

    return (len - remaining);
}

static void print_ips(struct addrinfo *lst) {
    /* IPv4 */
    char ipv4[INET_ADDRSTRLEN];
    struct sockaddr_in *addr4;
    //uint_16 port;
    /* IPv6 */
    char ipv6[INET6_ADDRSTRLEN];
    struct sockaddr_in6 *addr6;
    
    for (; lst != NULL; lst = lst->ai_next) {
        if (lst->ai_addr->sa_family == AF_INET) {
            addr4 = (struct sockaddr_in *) lst->ai_addr;
	    //port = htons (&addr4->sin_port);
	    inet_ntop(AF_INET, &addr4->sin_addr, ipv4, INET_ADDRSTRLEN);
            printf("IP: %s, port: %i\n", ipv4, addr4->sin_port);
        }
        else if (lst->ai_addr->sa_family == AF_INET6) {
            addr6 = (struct sockaddr_in6 *) lst->ai_addr;
	    //port = htons (&addr6->sin6_port);
            inet_ntop(AF_INET6, &addr6->sin6_addr, ipv6, INET6_ADDRSTRLEN);
            printf("IP: %s, port: %i\n", ipv6, addr6->sin6_port);
        }
    }
}

static void print_ips2(struct sockaddr_storage *addr_in) {
  char host[NI_MAXHOST];
  //getnameinfo((struct sockaddr *)addr_in, addr_in->ss_len, host, sizeof(host), NULL, 0, NI_NUMERICHOST);
  
  if (addr_in->ss_family == AF_INET) {
    char ipv4[INET_ADDRSTRLEN];
    struct sockaddr_in *addr4 = (struct sockaddr_in *) addr_in;
    inet_ntop(AF_INET, &addr4->sin_addr, ipv4, INET_ADDRSTRLEN);
    //std::cout<<"host is "<<host<<std::endl;
    printf("IP: %s, port: %i\n", ipv4, addr4->sin_port);
  }

  //  std::cout<<"host is "<<host<<std::endl;
}

static __inline__ uint64_t getCurrentCycle(void)
{
#if defined(__x86_64__) || defined(__amd64__)
  uint64_t high, low;
  asm volatile("rdtscp" : "=a" (low), "=d" (high));
  asm volatile("cpuid" ::: "%rax", "%rbx", "%rcx", "%rdx");
   return (high << 32) | low;
#elif defined(__aarch64__)
   // System timer of ARMv8 runs at a different frequency than the CPU's.
  // The frequency is fixed, typically in the range 1-50MHz.  It can because
  // read at CNTFRQ special register.  We assume the OS has set up
  // the virtual timer properly.
  uint64_t virtual_timer_value;
  asm volatile("mrs %0, cntvct_el0" : "=r"(virtual_timer_value));
  return virtual_timer_value;
#endif
  
}

static uint8_t hybridLock(pthread_mutex_t *theLock, uint64_t spinCycle) {
    // uint64_t i=0;
  uint64_t spinStart = getCurrentCycle();
  uint64_t spinNow = getCurrentCycle();
  // i = spinNow -spinStart;
  if(pthread_mutex_trylock(theLock)==0){
    // printf("got lock after: spin[%lu]\n", i);
    return (uint8_t)1;
  }
  while ( !((spinNow -spinStart)/ spinCycle)) {
    if(pthread_mutex_trylock(theLock)==0){
      // printf("got lock after: spin[%lu]\n", i);
      return (uint8_t)1;
    }
    spinNow = getCurrentCycle();
  }

  pthread_mutex_lock(theLock);
  spinNow = getCurrentCycle();
  return (uint8_t)0;
}


#endif
