/** $lic$
 * Copyright (C) 2016-2017 by Massachusetts Institute of Technology
 *
 * This file is part of TailBench.
 *
 * If you use this software in your research, we request that you reference the
 * TaiBench paper ("TailBench: A Benchmark Suite and Evaluation Methodology for
 * Latency-Critical Applications", Kasture and Sanchez, IISWC-2016) as the
 * source in any publications that use this software, and that you send us a
 * citation of your work.
 *
 * TailBench is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 */

#ifndef __MSGS_H
#define __MSGS_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

//     imgdnn xapian masstree specjbb silo shore moses
// REQ   13    11      0        4      2     2     4
// RESP    4    12     0        6      2     2     4
// 1 << 13 for imgdnn, 1 << 11 for xapian
// 1 << 6 for masstree, 1 << 1 specjbb
// 1 << 1 for silo, 1 << 2 for shore

// RESP
// 1 << 5 for specjbb, 1 << 2 for silo

const int MAX_REQ_BYTES  = 1 << 2; // 1 MB
const int MAX_RESP_BYTES = 1 << 2; // 1 MB

enum ResponseType { RESPONSE, ROI_BEGIN, DUMP, FINISH };

struct Request {
    uint64_t id;
    uint64_t genNs;
    uint64_t sentNs;
    size_t len;
    char data[MAX_REQ_BYTES];
};

struct Response {
    ResponseType type;
    uint64_t id;
    uint64_t svcNs;
    uint64_t rTcpNs;
    uint64_t rMutexNs;
    uint64_t tMutexNs;
  uint8_t tSpinGet;
  uint8_t rSpinGet;
  size_t len;
    char data[MAX_RESP_BYTES];
};

#endif
