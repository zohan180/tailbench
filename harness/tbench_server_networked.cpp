/** $lic$
 * Copyright (C) 2016-2017 by Massachusetts Institute of Technology
 *
 * This file is part of TailBench.
 *
 * If you use this software in your research, we request that you reference the
 * TaiBench paper ("TailBench: A Benchmark Suite and Evaluation Methodology for
 * Latency-Critical Applications", Kasture and Sanchez, IISWC-2016) as the
 * source in any publications that use this software, and that you send us a
 * citation of your work.
 *
 * TailBench is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 */

#include "tbench_server.h"

#include <algorithm>
#include <atomic>
#include <vector>

#include "helpers.h"
#include "server.h"

#include <assert.h>
#include <errno.h>
#include <netinet/tcp.h>
#include <string.h>
#include <sys/select.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <string>

/*******************************************************************************
 * NetworkedServer
 *******************************************************************************/
NetworkedServer::NetworkedServer(int nthreads, std::string ip, int port, \
				 int nclients, int lockLimit) 
    : Server(nthreads)
{
    pthread_mutex_init(&sendLock, nullptr);
    pthread_mutex_init(&recvLock, nullptr);

    reqbuf = new Request[nthreads]; 
    spinlockMax = lockLimit;
#if defined(__x86_64__) || defined(__amd64__)
    // this number is based on the system running now.
    // maybe a better to get this number from compile environment
    spinFreq = 3400000000;
#elif defined(__aarch64__)
  // System timer of ARMv8 runs at a different frequency than the CPU's.
  // The frequency is fixed, typically in the range 1-50MHz.  It can because
  // read at CNTFRQ special register.  We assume the OS has set up
  // the virtual timer properly.
    asm volatile("mrs %0, cntfrq_el0" : "=r"(spinFreq));
#endif
    std::cout<<"spinFreq is "<<spinFreq<<std::endl;
    activeFds.resize(nthreads);

    recvClientHead = 0;
    
    // Get address info
    int status;
    struct addrinfo hints;
    struct addrinfo* servInfo;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    std::stringstream portstr;
    portstr << port;

    const char* ipstr = (ip.size() > 0) ? ip.c_str() : nullptr;

    std::cout << "ipstr get: " << ipstr << std::endl;

    if ((status = getaddrinfo(ipstr, portstr.str().c_str(), &hints, &servInfo))\
            != 0) {
        std::cerr << "getaddrinfo() failed: " << gai_strerror(status) \
            << std::endl;
        exit(-1);
    }

    // Create listening socket
    int listener = socket(servInfo->ai_family, servInfo->ai_socktype, \
            servInfo->ai_protocol);
    if (listener == -1) {
        std::cerr << "socket() failed: " << strerror(errno) << std::endl;
        exit(-1);
    }

    int yes = 1;
    if (setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) \
            == -1)  {
        std::cerr << "setsockopt() failed: " << strerror(errno) << std::endl;
        exit(-1);
    }

    if (bind(listener, servInfo->ai_addr, servInfo->ai_addrlen) == -1) {
        std::cerr << "bind() failed: " << strerror(errno) << std::endl;
        exit(-1);
    }

    if (listen(listener, 10) == -1) {
        std::cerr << "listen() failed: " << strerror(errno) << std::endl;
        exit(-1);
    }

    // Establish connections with clients
    struct sockaddr_storage clientAddr;
    socklen_t clientAddrSize;

    for (int c = 0; c < nclients; ++c) {
        clientAddrSize = sizeof(clientAddr);
        memset(&clientAddr, 0, clientAddrSize);
        int clientFd = accept(listener, \
                reinterpret_cast<struct sockaddr*>(&clientAddr), \
                &clientAddrSize);

        if (clientFd == -1) {
            std::cerr << "accept() failed: " << strerror(errno) << std::endl;
            exit(-1);
        }

	int nodelay = 1;
	if (setsockopt(clientFd, IPPROTO_TCP, TCP_NODELAY, 
		       reinterpret_cast<char*>(&nodelay), sizeof(nodelay)) == -1) {
	  std::cerr << "setsockopt(TCP_NODELAY) failed: " << strerror(errno) \
		    << std::endl;
	  exit(-1);
	}
	
	print_ips2(&clientAddr);
        clientFds.push_back(clientFd);
    }
}

NetworkedServer::~NetworkedServer() {
    delete reqbuf;
}

void NetworkedServer::removeClient(int fd) {
    auto it = std::find(clientFds.begin(), clientFds.end(), fd);
    clientFds.erase(it);
}

bool NetworkedServer::checkRecv(int recvd, int expected, int fd) {
    bool success = false;
    if (recvd == 0) { // Client exited
        std::cerr << "Client left, removing" << std::endl;
        removeClient(fd);
        success = false;
    } else if (recvd == -1) {
        std::cerr << "recv() failed: " << strerror(errno) \
            << ". Exiting" << std::endl;
        exit(-1);
    } else {
        if (recvd != expected) {
            std::cerr << "ERROR! recvd = " << recvd << ", expected = " \
                << expected << std::endl;
            exit(-1);
        }
        // assert(recvd == expected);
        success = true;
    }

    return success;
}

size_t NetworkedServer::recvReq(int id, void** data) {
    uint64_t curRecvLock = getCurNs();
    //pthread_mutex_lock(&recvLock);
    uint8_t spinGet = hybridLock(&recvLock,spinlockMax);

    //while(pthread_mutex_trylock(&recvLock)){
      //just keep on trying until get it
    //}
    uint64_t curTcpNs;
    bool success = false;
    Request* req;
    int fd = -1;

    while (!success && clientFds.size() > 0) {
      curTcpNs = getCurNs();
      int maxFd = -1;
        fd_set readSet;
        FD_ZERO(&readSet);
        for (int f : clientFds) {
            FD_SET(f, &readSet);
            if (f > maxFd) maxFd = f;
        }
        int ret = select(maxFd + 1, &readSet, nullptr, nullptr, nullptr);
        if (ret == -1) {
            std::cerr << "select() failed: " << strerror(errno) << std::endl;
            exit(-1);
        }
	
        fd = -1;

        for (size_t i = 0; i < clientFds.size(); ++i) {
            size_t idx = (recvClientHead + i) % clientFds.size();
            if (FD_ISSET(clientFds[idx], &readSet)) {
                fd = clientFds[idx];
                break;
            }
        }

        recvClientHead = (recvClientHead + 1) % clientFds.size();

        assert(fd != -1);

        int len = sizeof(Request) - MAX_REQ_BYTES; // Read request header first

        req = &reqbuf[id];
	//curTcpNs = getCurNs();
	int recvd = recvfull(fd, reinterpret_cast<char*>(req), len, 0);
        success = checkRecv(recvd, len, fd);
        if (!success) continue;
            
        recvd = recvfull(fd, req->data, req->len, 0);
        success = checkRecv(recvd, req->len, fd);
        if (!success) continue;
    }

    pthread_mutex_unlock(&recvLock);
    
    if (clientFds.size() == 0) {
        std::cerr << "All clients exited. Server finishing" << std::endl;
        exit(0);
    } else {
        uint64_t curSvcNs = getCurNs();
	reqInfo[id].id = req->id;
        reqInfo[id].recvLockNs = curRecvLock;
        reqInfo[id].tcpNs   = curTcpNs;
        reqInfo[id].startNs = curSvcNs;
	reqInfo[id].rSpinGet = spinGet;
        activeFds[id] = fd;
        *data = reinterpret_cast<void*>(&req->data);
    }

    //    pthread_mutex_unlock(&recvLock);

    return req->len;
};

void NetworkedServer::sendResp(int id, const void* data, size_t len) {
    uint64_t finishSvcNs = getCurNs();
    Response* resp = new Response();
    
    resp->type = RESPONSE;
    resp->id = reqInfo[id].id;
    resp->len = len;
    memcpy(reinterpret_cast<void*>(&resp->data), data, len);

    int fd = activeFds[id];
    int totalLen = sizeof(Response) - MAX_RESP_BYTES + len;
    assert(finishSvcNs > reqInfo[id].startNs);
    resp->svcNs = finishSvcNs - reqInfo[id].startNs;
    resp->rTcpNs = finishSvcNs - reqInfo[id].tcpNs;
    resp->rMutexNs = finishSvcNs - reqInfo[id].recvLockNs;
    

    //while(pthread_mutex_trylock(&sendLock)){
        //just keep on trying until get it
    //}
    uint8_t spinGet = hybridLock(&sendLock,spinlockMax);
    //    pthread_mutex_lock(&sendLock);

    uint64_t sentLockNs = getCurNs();
    resp->tMutexNs = sentLockNs - reqInfo[id].recvLockNs;
    resp->tSpinGet = spinGet;
    resp->rSpinGet = reqInfo[id].rSpinGet;
    // resp->sentLockTime = sentLockNs - finishSvcNs;
    int sent = sendfull(fd, reinterpret_cast<const char*>(resp), totalLen, 0);
 
    assert(sent == totalLen);

    ++finishedReqs;
    if (finishedReqs == warmupReqs) {
        resp->type = ROI_BEGIN;
	    std::cout << "finished warmup." << std::endl;
        for (int fd : clientFds) {
            totalLen = sizeof(Response) - MAX_RESP_BYTES;
            sent = sendfull(fd, reinterpret_cast<const char*>(resp), totalLen, 0);
            assert(sent == totalLen);
        }
    } else if (finishedReqs == warmupReqs + maxReqs) {
      resp->type = FINISH;
      std::cout << "finished warmup + ROI." << std::endl;
      for (int fd : clientFds) {
	totalLen = sizeof(Response) - MAX_RESP_BYTES;
	sent = sendfull(fd, reinterpret_cast<const char*>(resp), totalLen, 0);
	//assert(sent == totalLen);
      }
    }
    
    delete resp;
    
    pthread_mutex_unlock(&sendLock);
}

void NetworkedServer::finish() {
    //pthread_mutex_lock(&sendLock);
    while(pthread_mutex_trylock(&sendLock)){
        //just keep on trying until get it
    }
    Response* resp = new Response();
    resp->type = FINISH;

    for (int fd : clientFds) {
        int len = sizeof(Response) - MAX_RESP_BYTES;
        int sent = sendfull(fd, reinterpret_cast<const char*>(resp), len, 0);
        assert(sent == len);
    }

    delete resp;
    
    pthread_mutex_unlock(&sendLock);
}

/*******************************************************************************
 * Per-thread State
 *******************************************************************************/
__thread int tid;

/*******************************************************************************
 * Global data
 *******************************************************************************/
std::atomic_int curTid;
NetworkedServer* server;

/*******************************************************************************
 * API
 *******************************************************************************/
void tBenchServerInit(int nthreads) {
    curTid = 0;
    std::string serverurl = getOpt<std::string>("TBENCH_SERVER", "localhost");
    int serverport = getOpt<int>("TBENCH_SERVER_PORT", 8080);
    int nclients = getOpt<int>("TBENCH_NCLIENTS", 1);
    int spinLimit = getOpt<int>("TBENCH_SPINLIMIT", 1);
    server = new NetworkedServer(nthreads, serverurl, serverport, nclients, spinLimit);
}

void tBenchServerThreadStart() {
    tid = curTid++;
}

void tBenchServerFinish() {
    server->finish();
}

size_t tBenchRecvReq(void** data) {
    return server->recvReq(tid, data);
}

void tBenchSendResp(const void* data, size_t size) {
    return server->sendResp(tid, data, size);
}

