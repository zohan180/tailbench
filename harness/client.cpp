/** $lic$
 * Copyright (C) 2016-2017 by Massachusetts Institute of Technology
 *
 * This file is part of TailBench.
 *
 * If you use this software in your research, we request that you reference the
 * TaiBench paper ("TailBench: A Benchmark Suite and Evaluation Methodology for
 * Latency-Critical Applications", Kasture and Sanchez, IISWC-2016) as the
 * source in any publications that use this software, and that you send us a
 * citation of your work.
 *
 * TailBench is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 */

#include "client.h"
#include "helpers.h"
#include "tbench_client.h"

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <sys/select.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <unistd.h>

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

/*******************************************************************************
 * Client
 *******************************************************************************/

Client::Client(int _nthreads) {
    status = INIT;

    nthreads = _nthreads;
    pthread_mutex_init(&lock, nullptr);
    pthread_barrier_init(&barrier, nullptr, nthreads);
    
    minSleepNs = getOpt("TBENCH_MINSLEEPNS", 0);
    seed = getOpt("TBENCH_RANDSEED", 0);
    lambdaWarmup = getOpt<double>("TBENCH_QPS_WARMUP", 1000.0) * 1e-9;
    lambdaROI = getOpt<double>("TBENCH_QPS", 1000.0) * 1e-9;

    dist = nullptr; // Will get initialized in startReq()

    startedReqs = 0;

    queueTimes.reserve(400000);
    svcTimes.reserve(400000);
    rTcpTimes.reserve(400000);
    rMutexTimes.reserve(400000);
    tMutexTimes.reserve(400000);
    sjrnTimes.reserve(400000);
    rSpinHit.reserve(400000);
    tSpinHit.reserve(400000);
    tBenchClientInit();
}

Request* Client::startReq() {
    if (status == INIT) {
        pthread_barrier_wait(&barrier); // Wait for all threads to start up

        pthread_mutex_lock(&lock);
        //while(pthread_mutex_trylock(&lock)){
        //just keep on trying until get it
        //}

        if (!dist) {
            uint64_t curNs = getCurNs();
            //            dist = new ExpDist(lambdaROI, seed, curNs);
            dist = new ExpDist(lambdaWarmup, seed, curNs);
            status = WARMUP;

            pthread_barrier_destroy(&barrier);
            pthread_barrier_init(&barrier, nullptr, nthreads);
        }

        pthread_mutex_unlock(&lock);

        pthread_barrier_wait(&barrier);
    }

    pthread_mutex_lock(&lock);
    //while(pthread_mutex_trylock(&lock)){
        //just keep on trying until get it
    //}

    Request* req = new Request();
    size_t len = tBenchClientGenReq(&req->data);
    req->len = len;

    req->id = startedReqs++;
    req->genNs = dist->nextArrivalNs();
    inFlightReqs[req->id] = req;

    pthread_mutex_unlock(&lock);

    uint64_t curNs = getCurNs();

    if (curNs < req->genNs) {
        sleepUntil(std::max(req->genNs, curNs + minSleepNs));
    }

    return req;
}

void Client::finiReq(Response* resp) {
    pthread_mutex_lock(&lock);
    //while(pthread_mutex_trylock(&lock)){
        //just keep on trying until get it
    //}

    auto it = inFlightReqs.find(resp->id);
    assert(it != inFlightReqs.end());
    Request* req = it->second;

    if (status == ROI || status == WARMUP) {
        uint64_t curNs = getCurNs();

        assert(curNs > req->genNs);

        uint64_t sjrn = curNs - req->genNs;
        //assert(sjrn >= resp->svcNs);
        //uint64_t qtime = sjrn - resp->svcNs;
        uint64_t qtime = req->sentNs - req->genNs;
        queueTimes.push_back(qtime);
        svcTimes.push_back(resp->svcNs);
        rTcpTimes.push_back(resp->rTcpNs);
        rMutexTimes.push_back(resp->rMutexNs);
	tMutexTimes.push_back(resp->tMutexNs);
        sjrnTimes.push_back(sjrn);
        rSpinHit.push_back((uint64_t)(resp->rSpinGet));
        tSpinHit.push_back((uint64_t)(resp->tSpinGet));
    }

    delete req;
    inFlightReqs.erase(it);
    pthread_mutex_unlock(&lock);
}

void Client::_startRoi() {
    assert(status == WARMUP);
    status = ROI;
    //dump a gem5 checkpoint
    // if(system("/sbin/m5 checkpoint") == -1){
    //   std::cerr<<"checkpoint creating wrong!"<<std::endl;
    // }

    //if(system("rm lats.bin") == -1){
      //std::cerr<<"lats.bin not existed"<<std::endl;
    //}

    // seperate warmup lambda and roi lambda
    uint64_t curNs = getCurNs();
    dist = new ExpDist(lambdaROI, seed, curNs);
    std::cout<<"dont clear, starting new QPS of "<< lambdaROI<<std::endl;
    //return;
    queueTimes.clear();
    svcTimes.clear();
    rTcpTimes.clear();
    rMutexTimes.clear();
    tMutexTimes.clear();
    sjrnTimes.clear();
    rSpinHit.clear();
    tSpinHit.clear();
}

void Client::startRoi() {
    pthread_mutex_lock(&lock);
    //while(pthread_mutex_trylock(&lock)){
        //just keep on trying until get it
    //}
    _startRoi();
    pthread_mutex_unlock(&lock);
}

void Client::dumpStats() {
    std::cout << "dumpStats called"<<std::endl;
    if(status==WARMUP){
      std::cout << "but warmup state...."<<std::endl;
      return;
    }
    
    int outID = getpid();
    std::string outName = "lats.bin"+std::to_string(outID);
    std::ofstream out(outName, std::ios::out | std::ios::binary | std::ios::app);
    int reqs = sjrnTimes.size();
    std::cout << "dumpStats called, reqs:"<<reqs<<std::endl;
    for (int r = 0; r < reqs; ++r) {
        out.write(reinterpret_cast<const char*>(&queueTimes[r]), 
                    sizeof(queueTimes[r]));
        out.write(reinterpret_cast<const char*>(&svcTimes[r]), 
                    sizeof(svcTimes[r]));
        out.write(reinterpret_cast<const char*>(&rTcpTimes[r]),
                    sizeof(rTcpTimes[r]));
	out.write(reinterpret_cast<const char*>(&rMutexTimes[r]),
                    sizeof(rMutexTimes[r]));
	out.write(reinterpret_cast<const char*>(&tMutexTimes[r]),
                    sizeof(tMutexTimes[r]));
        out.write(reinterpret_cast<const char*>(&sjrnTimes[r]), 
                    sizeof(sjrnTimes[r]));
        out.write(reinterpret_cast<const char*>(&rSpinHit[r]),
                    sizeof(rSpinHit[r]));
        out.write(reinterpret_cast<const char*>(&tSpinHit[r]),
                    sizeof(tSpinHit[r]));
    }
    out.close();
    //clear out everytime dump stats
    //write data output to local
    // if(system("/sbin/m5 writefile lats.bin") == -1){
    //   std::cerr<<"writefile wrong!"<<std::endl;
    // }
    queueTimes.clear();
    svcTimes.clear();
    rTcpTimes.clear();
    rMutexTimes.clear();
    tMutexTimes.clear();
    sjrnTimes.clear();
    rSpinHit.clear();
    tSpinHit.clear();
}

/*******************************************************************************
 * Networked Client
 *******************************************************************************/
NetworkedClient::NetworkedClient(int nthreads, std::string serverip, 
				 int serverport, std::string clientip, int clientport) : Client(nthreads)
{
    pthread_mutex_init(&sendLock, nullptr);
    pthread_mutex_init(&recvLock, nullptr);

    //sendCnt = 0;
    //warmupQuests = warmupQs;
    //roiQuests = roiQs;
    //startTime = getCurNs();
    //std::cout<<"CLIENT: init at "<< getCurNs()/1000000 <<std::endl;
    
    // Get address info
    int status;
    struct addrinfo hints;
    struct addrinfo* servInfo;

    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    std::stringstream portstr;
    portstr << serverport;
    
    const char* serverStr = serverip.size() ? serverip.c_str() : nullptr;

    if ((status = getaddrinfo(serverStr, portstr.str().c_str(), &hints, 
                    &servInfo)) != 0) {
        std::cerr << "getaddrinfo() failed: " << gai_strerror(status) \
            << std::endl;
        exit(-1);
    }

    serverFd = socket(servInfo->ai_family, servInfo->ai_socktype, \
            servInfo->ai_protocol);
    if (serverFd == -1) {
        std::cerr << "socket() failed: " << strerror(errno) << std::endl;
        exit(-1);
    }

    int yes =1;
    if (setsockopt(serverFd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) \
	    == -1)  {
        std::cerr << "setsockopt() failed: " << strerror(errno) << std::endl;
        exit(-1);
    }
    
    struct sockaddr_in my_addr, peer_addr;
    my_addr.sin_family = AF_INET;
    my_addr.sin_addr.s_addr = INADDR_ANY;
    // This ip address will change according to the machine
    const char* clientStr = clientip.size() ? clientip.c_str() : nullptr;
    my_addr.sin_addr.s_addr = inet_addr(clientStr);     
    std::stringstream clientportstr;
    //int clientportnum = 12345;
    //clientportstr << clientportnum;
    my_addr.sin_port = htons(clientport);
    //my_addr.sin_port = clientportstr;
    if (clientport != 1000){
      if (bind(serverFd, (struct sockaddr*) &my_addr, sizeof(my_addr)) == 0)
        printf("Binded Success\n");
      else
        printf("Unable to bind\n");
    }
    //print_ips2( &my_addr);
    printf("Binded Success.\n");
    if (connect(serverFd, servInfo->ai_addr, servInfo->ai_addrlen) == -1) {
        std::cerr << "connect() failed: " << strerror(errno) << std::endl;
        exit(-1);
    }

    int nodelay = 1;
    //    int yes =1;
    //    if (setsockopt(serverFd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) \
//            == -1)  {
    //        std::cerr << "setsockopt() failed: " << strerror(errno) << std::endl;
    //	exit(-1);
    //    }
    if (setsockopt(serverFd, IPPROTO_TCP, TCP_NODELAY,
		   reinterpret_cast<char*>(&nodelay), sizeof(nodelay)) == -1) {
      std::cerr << "setsockopt(TCP_NODELAY) failed: " << strerror(errno) \
		<< std::endl;
      exit(-1);
    }
}

bool NetworkedClient::send(Request* req) {
    pthread_mutex_lock(&sendLock);
    //while(pthread_mutex_trylock(&sendLock)){
        //just keep on trying until get it
    //}
    //sendCnt++;

    req->sentNs = getCurNs();
    int len = sizeof(Request) - MAX_REQ_BYTES + req->len;
    int sent = sendfull(serverFd, reinterpret_cast<const char*>(req), len, 0);
    if (sent != len) {
        error = strerror(errno);
    }
    /****
    if (sendCnt == warmupQuests){
      std::cout<<"CLIENT: have sent enough warmup requests at: "<< (getCurNs()-startTime)/1000000 <<std::endl;
    }

    if (sendCnt == warmupQuests+roiQuests){
      std::cout<<"CLIENT: have sent enough roi requests at "<< (getCurNs()-startTime)/1000000 <<std::endl;
    }
    ********/

    pthread_mutex_unlock(&sendLock);

    return (sent == len);
}

bool NetworkedClient::recv(Response* resp) {
    pthread_mutex_lock(&recvLock);
    //while(pthread_mutex_trylock(&recvLock)){
        //just keep on trying until get it
    //}
    //std::cout << "here in client.cpp: recv" << std::endl;
    int len = sizeof(Response) - MAX_RESP_BYTES; // Read request header first
    int recvd = recvfull(serverFd, reinterpret_cast<char*>(resp), len, 0);
    if (recvd != len) {
        error = strerror(errno);
        return false;
    }

    if (resp->type == RESPONSE) {
        recvd = recvfull(serverFd, reinterpret_cast<char*>(&resp->data), \
                resp->len, 0);

        if (static_cast<size_t>(recvd) != resp->len) {
            error = strerror(errno);
            return false;
        }
    }

    pthread_mutex_unlock(&recvLock);

    return true;
}

void NetworkedClient::myClose() {
  std::cout<<"close a client here"<<std::endl;
  close(serverFd);
}
