#!/usr/bin/python3
import warnings
import sys
import os
import numpy as np
import time
from scipy import stats

warnings.simplefilter(action='ignore', category=FutureWarning)

class Lat(object):
    def __init__(self, fileName):
        f = open(fileName, 'rb')
        a = np.fromfile(f, dtype=np.uint64)
        self.reqTimes = a.reshape((a.shape[0]//8, 8))
        f.close()

    def parseQueueTimes(self):
        return self.reqTimes[:, 0]

    def parseSvcTimes(self):
        return self.reqTimes[:, 1]
    
    def parseTcpTimes(self):
        return self.reqTimes[:, 2]

    def parseRMutexTimes(self):
        return self.reqTimes[:, 3]

    def parseTMutexTimes(self):
        return self.reqTimes[:, 4]
    
    def parseSojournTimes(self):
        return self.reqTimes[:, 5]

    def parseRSpinHit(self):
        return self.reqTimes[:, 6]
    def parseTSpinHit(self):
        return self.reqTimes[:, 7]

if __name__ == '__main__':
    def getLatPct(latsFile):
        assert os.path.exists(latsFile)
        latsObj = Lat(latsFile)

        qTimes = np.divide(latsObj.parseQueueTimes(), 1000000)
        svcTimes = np.divide(latsObj.parseSvcTimes(), 1000000)
        rTcpTimes = np.divide(latsObj.parseTcpTimes(), 1000000)
        rMutexTimes = np.divide(latsObj.parseRMutexTimes(), 1000000)
        tMutexTimes = np.divide(latsObj.parseTMutexTimes(), 1000000)
        sjrnTimes = np.divide(latsObj.parseSojournTimes(), 1000000)

        rSpinHit = latsObj.parseRSpinHit()
        tSpinHit = latsObj.parseTSpinHit()
        
        f = open('lats.txt','w')

        f.write('%12s | %12s | %12s | %12s | %12s | %12s | %12s | %12s \n\n' \
                % ('QueueTimes', 'ServiceTimes', 'TcpTimes', 'rMutexTimes', \
                   'tMutexTimes', 'SojournTimes', 'rSpinHit', 'tSpinHit'))

        for (q, svc, tcp, rmutex, tmutex, sjrn,rspin,tspin) \
            in zip(qTimes, svcTimes, rTcpTimes, rMutexTimes, tMutexTimes, sjrnTimes, rSpinHit, tSpinHit):
            f.write("%12s | %12s | %12s | %12s | %12s | %12s | %12s | %12s \n" \
                    % ('%.3f' % q, '%.3f' % svc, '%.3f' % tcp, \
                       '%.3f' % rmutex, '%.3f' % tmutex, '%.3f' % sjrn, \
                       '%.3f' % rspin, '%.3f' % tspin))
        f.close()

        qMean = np.mean(qTimes)
        sjrn95 = np.percentile(sjrnTimes, 95)
        sjrn99 = np.percentile(sjrnTimes, 99)
        sjrnMean = np.mean(sjrnTimes)
        
        svc95 = np.percentile(svcTimes, 95)
        svc99 = np.percentile(svcTimes, 99)
        svcMean = np.mean(svcTimes)
        svcMax = np.max(svcTimes)
        
        rMutex95 = np.percentile(rMutexTimes, 95)
        rMutex99 = np.percentile(rMutexTimes, 99)
        rMutexMean = np.mean(rMutexTimes)
        rMutexMax = np.max(rMutexTimes)

        tMutex95 = np.percentile(tMutexTimes, 95)
        tMutex99 = np.percentile(tMutexTimes, 99)
        tMutexMean = np.mean(tMutexTimes)
        tMutexMax = np.max(tMutexTimes)
        
        tcp95 = np.percentile(rTcpTimes, 95)
        tcp99 = np.percentile(rTcpTimes, 99)
        tcpMean = np.mean(rTcpTimes)
        tcpMax = np.max(rTcpTimes)

        rspinRate = sum(rSpinHit)/len(rSpinHit)
        tspinRate = sum(tSpinHit)/len(rSpinHit)
        # print ("95th percentile latency %.3f ms | 99th latency %.3f ms | max latency %.3f ms | mean latency %.3f ms" \
        #         % (p95, p99, maxLat, meanLat))
        print ("%.3f %.3f %.3f | %.3f %.3f %.3f  %.3f | %.3f %.3f | %.3f %.3f | %.3f %.3f| %.3f %.3f" \
                % (sjrnMean, sjrn95, sjrn99, \
                   svcMean, svc95, svc99, svcMax, \
                   tcpMean, tcpMax, rMutexMean, \
                   rMutexMax, tMutexMean, tMutexMax, \
                   rspinRate, tspinRate
                ) )
        
    latsFile = sys.argv[1]
    getLatPct(latsFile)
        
