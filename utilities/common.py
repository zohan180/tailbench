import numpy as np
import pandas as pd

import os
from os.path import isfile, join
from os import listdir

import parse as myparse

# folderTodf, get data from all QPS
allIndex = ["sjrnMean", "sjrn95", "sjrn99", "svcMean", "svc95", "svc99", \
            "tcpMean", "tcp95", "tcp99", "rMutexMean", "rMutex95", "rMutex99", \
            "tMutexMean", "tMutex95", "tMutex99", "rSpinGet", "tSpinGet"]

timeName = ["queueTime", "svcTime", "tcpTime", \
            "rMutexTime", "tMutexTime", \
            "sjrnTime","rSpinGet", "tSpinGet"]

def folderTodf(basedir, bench, thread, bins, clients, qpss, skip):
    datadir = basedir+"/"+str(bench)+"-"+str(thread)
    results = np.empty((len(qpss), 17))
    rdx = 0
    for qps in qpss:
        # results[rdx]=myparse.getTailMultLatPct(datadir, skip*qps*thread/clients, qps, bins, clients)
        results[rdx]=myparse.getTailMultLatPct(datadir, skip*qps*thread/clients, qps, bins, clients)
        rdx = rdx + 1
    df = pd.DataFrame(results, columns=allIndex, index=qpss)
    return df

def feedThreadData(thread_data,bench,thread,qpsList,config,spin,bins,writeToCSV,csvBaseDir,rawDataDir,clientNum,skip):
    if bench not in thread_data:
        thread_data[bench] = {}
    if config not in thread_data[bench]:
        thread_data[bench][config] ={}
    # for name in allIndex:
    #     thread_data[bench][config][name]=pd.DataFrame()
    # for thread in threadList:
    basedir=rawDataDir+"irq%s-spin%s/"%(config,spin)
    # print(basedir)
    get = folderTodf(basedir, bench, thread, bins, clientNum, qpsList, skip)
    csv_name=bench+"-irq%s-%st-spin%s.csv"%(config,thread,spin)
    thread_data[bench][config][thread] = get
    if writeToCSV == True:
        print("wrote to :%s" % csv_name)
        get.to_csv(os.path.join(csvBaseDir,csv_name), index=True)

def plotqps(axs, mode, bench, qpss, threadNum, binNum, yrange, rawDataDir,skip):
    for qdx, qps in enumerate(qpss):
        for bdx in range(1,binNum+1):
            # latsFile=rawDataDir+mode+bench+"-"+str(tdx)+"/"+str(qps)+"/lats-1.bin%s"%(threadNum,qps,cdx)
            latsFile=os.path.join(rawDataDir+mode,"%s-%s/%s/lats-%s.bin1"%(bench,threadNum,qps,bdx))
            lat =  myparse.singleLat(latsFile, skip*qps*threadNum/binNum, None, None, None)
            data = pd.DataFrame(lat.reqTimes, columns=timeName)
            data["index"] = data.index
            data["time"] = data["index"]/(qps*threadNum/threadNum)
            data.plot.scatter(x="time", y="sjrnTime", ax=axs[qdx,bdx-1], color='r', title="svcTime at config: %s"%qps)
            data.plot.scatter(x="time", y="svcTime", ax=axs[qdx,bdx-1], color='g')
            
            if yrange is not None:
                axs[qdx,bdx-1].set_ylim(0,yrange)

            axs[qdx,bdx-1].axhline(data["svcTime"].mean(), color='g', linestyle='dashed', \
                    linewidth=1, label="svc Mean = "+'{:.3f}'.format(data["svcTime"].mean()/1000000))
            axs[qdx,bdx-1].axhline(data["sjrnTime"].mean(), color='r', linestyle='dashed', \
                    linewidth=1, label="rount Mean = "+'{:.3f}'.format(data["sjrnTime"].mean()/1000000))
            axs[qdx,bdx-1].legend(loc="upper left")

def plotWAGqps(axs, mode, bench, qpss, threadNum, yrange, rawDataDir, skip, bins):
    for qdx, qps in enumerate(qpss):
        for bdx in range(1,bins+1):
            # latsFile=rawDataDir+mode+bench+"-"+str(tdx)+"/"+str(qps)+"/lats-1.bin%s"%(threadNum,qps,bdx)
            latsFile=os.path.join(rawDataDir+mode,"%s-%s/%s/lats-%s.bin1"%(bench,threadNum,qps,bdx))
            lat =  myparse.singleLat(latsFile, skip*qps*threadNum, None, None, None)
            data = pd.DataFrame(lat.reqTimes, columns=timeName)
            data["index"] = data.index
            data["time"] = data["index"]/(qps*threadNum)
            data.plot.scatter(x="time", y="sjrnTime", ax=axs[qdx,bdx-1], color='r', title="svcTime at config: %s"%qps)
            data.plot.scatter(x="time", y="svcTime", ax=axs[qdx,bdx-1], color='g')
            
            if yrange is not None:
                axs[qdx,bdx-1].set_ylim(0,yrange)

            axs[qdx,bdx-1].axhline(data["svcTime"].mean(), color='g', linestyle='dashed', \
                    linewidth=1, label="svc Mean = "+'{:.3f}'.format(data["svcTime"].mean()/1000000))
            axs[qdx,bdx-1].axhline(data["sjrnTime"].mean(), color='r', linestyle='dashed', \
                    linewidth=1, label="rount Mean = "+'{:.3f}'.format(data["sjrnTime"].mean()/1000000))
            axs[qdx,bdx-1].legend(loc="upper left")

def plotbins(axs, mode, bench, qps, threadNum, clientNum, yrange, rawDataDir,skip):
    for bdx in range(1,5):
        for cdx in range(1,clientNum+1):
            tdx=threadNum
            # latsFile=rawDataDir+mode+bench+"-"+str(tdx)+"/"+str(qps)+"/lats-"+str(bdx)+".bin"+str(cdx)
            latsFile=os.path.join(rawDataDir+mode,"%s-%s/%s/lats-%s.bin%s"%(bench,threadNum,qps,bdx,cdx))
            lat =  myparse.singleLat(latsFile, skip*qps*tdx/clientNum, None, None, None)
            data = pd.DataFrame(lat.reqTimes, columns=timeName)
            data["index"] = data.index
            data["time"] = data["index"]/(qps*tdx/clientNum)
            data.plot.scatter(x="time", y="sjrnTime", ax=axs[bdx-1,cdx-1], color='r', \
                    title="svcTime at config: %s, bin%s, client%s"%(qps,bdx,cdx))
            data.plot.scatter(x="time", y="svcTime", ax=axs[bdx-1,cdx-1], color='g')
            if yrange is not None:
                axs[bdx-1,cdx-1].set_ylim(0,yrange)

            axs[bdx-1,cdx-1].axhline(data["svcTime"].mean(), color='g', linestyle='dashed', \
                    linewidth=1, label="svc Mean = "+'{:.3f}'.format(data["svcTime"].mean()/1000000))
            axs[bdx-1,cdx-1].axhline(data["sjrnTime"].mean(), color='r', linestyle='dashed', \
                    linewidth=1, label="rount Mean = "+'{:.3f}'.format(data["sjrnTime"].mean()/1000000))
            axs[bdx-1,cdx-1].legend(loc="upper left")

def getSpinData(spin_data,bench,config,thread,qpsList,spinList,writeToCSV,csvBaseDir,rawDataDir,bins,clientNum):
    skip=0
    if bench not in spin_data:
        spin_data[bench] = {}
    if config not in  spin_data[bench]:
        spin_data[bench][config] ={}
    for name in allIndex:
        spin_data[bench][config][name]=pd.DataFrame()
    for spin in spinList:
        basedir=rawDataDir+"irq%s-spin%s/"%(config,spin)
        print(basedir)
        get = folderTodf(basedir, bench, thread, bins, clientNum, qpsList, skip)
        for name in allIndex:
            spin_data[bench][config][name] = pd.concat([spin_data[bench][config][name],get[name]],axis=1)
    for name in allIndex:
        spin_data[bench][config][name].columns = spinList
        csv_name=os.path.join(csvBaseDir,bench+"-irq%s-%st-spin-%s.csv"%(config,thread,name))
        # print(csv_name)
        if writeToCSV == True:
            spin_data[bench][config][name].to_csv(csv_name, index=True)
