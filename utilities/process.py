
import numpy as np
import pandas as pd

np.set_printoptions(suppress=True,linewidth=150,threshold=150)
np.set_printoptions(precision=6)
pd.set_option("display.precision", 6)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

from common import *

tailCSVDir="/home/heng/tailbench/results/csv-napi"
tailDataDir="/home/heng/tailbench/results-"

thread_data={}

bench="silo"
thread=1
qpss=list(range(36000,41000,1000))
#cpulist=[0,1,2,3,4,5]
cpulist=[6,7,8,9,10,11]
for cpu in [6]:
    for rx in cpulist:
        for tx in cpulist:
            config="%s-%s"%(rx,tx)
            feedThreadData(thread_data,bench,thread,qpss,config,1,4,True,tailCSVDir+"/%s"%cpu,tailDataDir+"napi-%s-1c/"%cpu,thread,0)
            config="%s-%s-%s-%s"%(rx,rx,tx,tx)
            feedThreadData(thread_data,bench,thread,qpss,config,1,4,True,tailCSVDir+"/%s"%cpu,tailDataDir+"napi-%s-1c/"%cpu,thread,0)
