#!/usr/bin/python3
import sys
import os
import numpy as np
from scipy import stats
import re
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

data = np.zeros((5*5*5, 8))
label = np.zeros((5*5*5, 4))

font = {'family': 'serif',
        'color':  'darkred',
        'weight': 'normal',
        'size': 22,
        }

def getData(latsFile):
	f = open(latsFile, 'r')
	lines = f.readlines()
	index = 0
	std = False

	for line in lines:
		row = re.findall('\d+\.\d+', line)
		if row:
			row = np.array(row).astype(np.float)
			if not std:
				data[index,0:4] = row
				std = True
			else:
				data[index,4:8] = row
				std = False
				index = index + 1
		else:
			qps_warmup = re.findall('\d+', line)
			label[index] = np.array(qps_warmup).astype(np.int)

	f.close()

def plotData():
	# plt.figure(figsize=(32, 32))
	# how many subplots, based on mean/95, PQS...?
	fig, ax = plt.subplots(2,3,figsize=(45,15))
	# fig.suptitle('Vertically stacked subplots')

	# plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')

	axisX1 = label.T[1]
	axisX2 = label.T[2]
	axisX3 = label.T[3]
	labelX1 = 'WARMUPQPS'
	labelX2 = 'MAXREQ'
	labelX3 = 'WARMUPREQ'
	labelY = 'latency in ms'

	ax[0,0].set_title('mean plot', fontdict=font)
	ax[0,0].set_xlabel(labelX1, fontdict=font)
	ax[0,0].set_ylabel(labelY, fontdict=font)
	ax[0,0].set_ylim(0, 3)

	ax[0,0].plot(axisX1, data.T[0],'rs',label="mean")  # Plot some data on the axes.
	ax[0,0].errorbar(axisX1, data.T[0], data.T[4], linestyle='None', marker='^')
	# ax1.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
	#            ncol=3, mode="expand", borderaxespad=0.)

	ax[1,0].set_title('95th plot', fontdict=font)
	ax[1,0].set_xlabel(labelX1, fontdict=font)
	ax[1,0].set_ylabel(labelY, fontdict=font)
	ax[1,0].set_ylim(0, 3)

	ax[1,0].plot(axisX1, data.T[1],'bs',label="95th")
	ax[1,0].errorbar(axisX1, data.T[1], data.T[5], linestyle='None', marker='^')
	# plt.errorbar(axisX, data.T[2], data.T[6], linestyle='None', marker='^')
		# ax2.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
	#            ncol=3, mode="expand", borderaxespad=0.)

	ax[0,1].set_title('mean plot', fontdict=font)
	ax[0,1].set_xlabel(labelX2, fontdict=font)
	ax[0,1].set_ylabel(labelY, fontdict=font)
	ax[0,1].set_ylim(0, 3)

	ax[0,1].plot(axisX2, data.T[0],'rs',label="mean")  # Plot some data on the axes.
	ax[0,1].errorbar(axisX2, data.T[0], data.T[4], linestyle='None', marker='^')

	ax[1,1].set_title('95th plot', fontdict=font)
	ax[1,1].set_xlabel(labelX2, fontdict=font)
	ax[1,1].set_ylabel(labelY, fontdict=font)
	ax[1,1].set_ylim(0, 3)

	ax[1,1].plot(axisX2, data.T[1],'bs',label="95th")
	ax[1,1].errorbar(axisX2, data.T[1], data.T[5], linestyle='None', marker='^')

	ax[0,2].set_title('mean plot', fontdict=font)
	ax[0,2].set_xlabel(labelX3, fontdict=font)
	ax[0,2].set_ylabel(labelY, fontdict=font)
	ax[0,2].set_ylim(0, 3)

	ax[0,2].plot(axisX3, data.T[0],'rs',label="mean")  # Plot some data on the axes.
	ax[0,2].errorbar(axisX3, data.T[0], data.T[4], linestyle='None', marker='^')

	ax[1,2].set_title('95th plot', fontdict=font)
	ax[1,2].set_xlabel(labelX3, fontdict=font)
	ax[1,2].set_ylabel(labelY, fontdict=font)
	ax[1,2].set_ylim(0, 3)

	ax[1,2].plot(axisX3, data.T[1],'bs',label="95th")
	ax[1,2].errorbar(axisX3, data.T[1], data.T[5], linestyle='None', marker='^')

	for ax in fig.get_axes():
		ax.label_outer()

	fig.savefig(outFile)

# latsFile = 'test.txt'
latsFile = sys.argv[1]
outFile = sys.argv[2]
getData(latsFile)
plotData()


# f = open('test.txt', 'r')
# w = open('out.txt', 'w')
# lines = f.readlines()
# data = np.zeros((5, 8))
# label = np.zeros((5, 4))
# index = 0
# std = False

# for line in lines:
# 	row = re.findall('\d+\.\d+', line)
# 	if row:
# 		row = np.array(row).astype(np.float)
# 		if not std:
# 			data[index,0:4] = row
# 			std = True
# 		else:
# 			data[index,4:8] = row
# 			std = False
# 			index = index + 1
# 	else:
# 		qps_warmup = re.findall('\d+', line)
# 		label[index] = np.array(qps_warmup).astype(np.int)

# w.writelines(str(output))
# w.close
# f.close()
# axisX = label.T[1]
# # plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')
# plt.plot(axisX, data.T[0],'r--',label="mean")  # Plot some data on the axes.
# plt.plot(axisX, data.T[1],'b--',label="95th")
# # plt.plot(axisX, data.T[2],'g--',label="99th")
# plt.xlabel('qps_warmup')
# plt.ylabel('latency in ms')
# plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
#            ncol=3, mode="expand", borderaxespad=0.)
# # stdMean = np.array([1.5, 2.6, 3.7, 4.6, 5.5])
# # std95th = 
# # std99th = 
# plt.errorbar(axisX, data.T[0], data.T[4], linestyle='None', marker='^')
# plt.errorbar(axisX, data.T[1], data.T[5], linestyle='None', marker='^')
# # plt.errorbar(axisX, data.T[2], data.T[6], linestyle='None', marker='^')

# plt.ylim(0, 3)
# # plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)
# # plt.show()
# # plt.close()
# plt.savefig('test.png')