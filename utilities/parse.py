#!/usr/bin/env python3
# import numpy as numpy
import numpy
import pandas as pd
import os
from scipy import stats
from os.path import isfile, join
from os import listdir

class oldLat(object):
    def __init__(self, filePath, skip=None, qps=None, binRange=None, cRange=None):
        f = open(filePath, 'rb')
        # print(filePath)
        a = numpy.fromfile(f, dtype=numpy.uint64)
        self.reqTimes = a.reshape((a.shape[0]//6, 6))[int(skip):,:]
        self.reqTimes[:, 4] = self.reqTimes[:, 4]-self.reqTimes[:, 3]
        self.reqTimes[:, 3] = self.reqTimes[:, 3]-self.reqTimes[:, 2]
        self.reqTimes[:, 2] = self.reqTimes[:, 2]-self.reqTimes[:, 1]
        f.close()

class singleLat(object):
    def __init__(self, filePath, skip=None, qps=None, binRange=None, cRange=None):
        f = open(filePath, 'rb')
        # print(filePath)
        a = numpy.fromfile(f, dtype=numpy.uint64)
        self.reqTimes = a.reshape((a.shape[0]//8, 8))[int(skip):,:]
        self.reqTimes[:, 4] = self.reqTimes[:, 4]-self.reqTimes[:, 3]
        self.reqTimes[:, 3] = self.reqTimes[:, 3]-self.reqTimes[:, 2]
        self.reqTimes[:, 2] = self.reqTimes[:, 2]-self.reqTimes[:, 1]
        f.close()

    def parseQueueTimes(self):
        return self.reqTimes[:, 0]

    def parseSvcTimes(self):
        return self.reqTimes[:, 1]

    def parseTcpTimes(self):
        return self.reqTimes[:, 2]-self.reqTimes[:, 1]
    def parseRMutexTimes(self):
        return self.reqTimes[:, 3]-self.reqTimes[:, 2]
    def parseTMutexTimes(self):
        return self.reqTimes[:, 4]-self.reqTimes[:, 3]

    def parseSojournTimes(self):
        return self.reqTimes[:, 5]
    
    def parseRSpinHit(self):
        return self.reqTimes[:, 6]
    def parseTSpinHit(self):
	    return self.reqTimes[:, 7]

class Lat(object):
    def __init__(self, filePath, skip=None, qps=None, binRange=None, cRange=None):
        # print("looking for: ", fileName)
        if qps is None:
            # single file
            f = open(filePath, 'rb')
            a = numpy.fromfile(f, dtype=numpy.uint64)
            self.reqTimes = a.reshape((a.shape[0]//8, 8))[int(skip):,:]
            f.close()
        else:
            # print("looking for: ", fileName)
            self.reqTimes = numpy.empty([0,0])
            for bidx in range(1,binRange+1):
                for cidx in range(1,cRange+1):
                    fileName = join(filePath,str(qps))+"/lats-"+str(bidx)+".bin"+str(cidx)
                    # print("looking for: ", fileName)
                    # fileName = join(filePath,'lats.bin')
                    if os.path.exists(fileName):
                        f = open(fileName, 'rb')
                        a = numpy.fromfile(f, dtype=numpy.uint64) # a will be 1-D array of all data
                        # self.reqTimes = numpy.append(self.reqTimes, a[:], axis=None)
                        try:
                            single = a.reshape((a.shape[0]//8, 8))[int(skip):,:] # trasnsfer to 2-D array, apply skip if needed
                            self.reqTimes = numpy.append(self.reqTimes, single.reshape((1,single.shape[0]*8)), axis=None)
                        except ValueError:
                            print("wrong with this: ", fileName)
                        f.close()
                    else:
                        print("not found in this dir :", fileName)
            self.reqTimes = self.reqTimes.reshape((self.reqTimes.shape[0]//8, 8))
            # self.reqTimes[:, 4] = self.reqTimes[:, 4]-self.reqTimes[:, 3]
            # self.reqTimes[:, 3] = self.reqTimes[:, 3]-self.reqTimes[:, 2]
            # self.reqTimes[:, 2] = self.reqTimes[:, 2]-self.reqTimes[:, 1]
            self.reqTimes[:, 4] = self.reqTimes[:, 4]
            self.reqTimes[:, 3] = self.reqTimes[:, 3]
            self.reqTimes[:, 2] = self.reqTimes[:, 2]
            if "wag" in filePath:
                # print("Found!")
                self.reqTimes[:, 5] = self.reqTimes[:, 5]  + 20000

    def parseQueueTimes(self):
        return self.reqTimes[:, 0]

    def parseSvcTimes(self):
        return self.reqTimes[:, 1]

    def parseTcpTimes(self):
        return self.reqTimes[:, 2]
    def parseRMutexTimes(self):
        return self.reqTimes[:, 3]
    def parseTMutexTimes(self):
        return self.reqTimes[:, 4]
    def parseSojournTimes(self):
        return self.reqTimes[:, 5]
    
    def parseRSpinHit(self):
        return self.reqTimes[:, 6]
    def parseTSpinHit(self):
	    return self.reqTimes[:, 7]

def getTailMultLatPct(latsFolder, skip=None, qps=None, binRange=None, clientRange=None):
    # print(latsFolder)
    assert os.path.exists(latsFolder),latsFolder+" not found"
    latsObj = Lat(latsFolder, skip, qps, binRange, clientRange)
    qTimes = numpy.divide(latsObj.parseQueueTimes(), 1000000)
    svcTimes = numpy.divide(latsObj.parseSvcTimes(), 1000000)
    tcpTimes = numpy.divide(latsObj.parseTcpTimes(), 1000000)
    rMutexTimes = numpy.divide(latsObj.parseRMutexTimes(), 1000000)
    tMutexTimes = numpy.divide(latsObj.parseTMutexTimes(), 1000000)
    sjrnTimes = numpy.divide(latsObj.parseSojournTimes(), 1000000)
    
    rSpinHit = latsObj.parseRSpinHit()
    tSpinHit = latsObj.parseTSpinHit()
    rspinRate = sum(rSpinHit)/len(rSpinHit)
    tspinRate = sum(tSpinHit)/len(rSpinHit)

    # spinTimes = latsObj.parseSpinTimes()
    sjrn95th = numpy.percentile(sjrnTimes, 95)
    sjrn99th = numpy.percentile(sjrnTimes, 99)
    sjrnMean = numpy.mean(sjrnTimes)
    sjrnMax = numpy.max(sjrnTimes)
    sjrn95Mean = sjrnTimes[sjrnTimes >= sjrn95th].mean()
    sjrn99Mean = sjrnTimes[sjrnTimes >= sjrn99th].mean()
    sjrnstd = stats.tstd(sjrnTimes)

    svc95th = numpy.percentile(svcTimes, 95)
    svc99th = numpy.percentile(svcTimes, 99)
    svcMean = numpy.mean(svcTimes)
    svcMax =  numpy.max(svcTimes)
    # svc95Mean = numpy.mean(svcTimes, (svc95th, svcMax))
    # svc99Mean = numpy.mean(svcTimes, (svc99th, svcMax))
    svc95Mean = svcTimes[svcTimes >= svc95th].mean()
    svc99Mean = svcTimes[svcTimes >= svc99th].mean()
    svcstd = stats.tstd(svcTimes)

    tcp95 = numpy.percentile(tcpTimes, 95)
    tcp99 = numpy.percentile(tcpTimes, 99)
    tcpMean = numpy.mean(tcpTimes)

    rMutex95 = numpy.percentile(rMutexTimes, 95)
    rMutex99 = numpy.percentile(rMutexTimes, 99)
    rMutexMean = numpy.mean(rMutexTimes)
    rMutexMax = numpy.max(rMutexTimes)

    tMutex95 = numpy.percentile(tMutexTimes, 95)
    tMutex99 = numpy.percentile(tMutexTimes, 99)
    tMutexMean = numpy.mean(tMutexTimes)
    tMutexMax = numpy.max(tMutexTimes)

    # print("change")
    # return numpy.asarray([sjrnMean, sjrn95Mean, sjrn99Mean, svcMean, svc95th, svc99th, \
    #     tcpMean, tcp95, tcp99, rMutexMean, rMutex95, rMutex99, tMutexMean, tMutex95, tMutex99, rspinRate, tspinRate])
    return numpy.asarray([sjrnMean, sjrn95th, sjrn99th, svcMean, svc95th, svc99th, \
        tcpMean, tcp95, tcp99, rMutexMean, rMutex95, rMutex99, tMutexMean, tMutex95, tMutex99, rspinRate, tspinRate])

class serverLat(object):
    def __init__(self, filePath, skip=None, qps=None, binRange=None, cRange=None):
        # print("looking for: ", fileName)
        if qps is None:
            # single file
            f = open(filePath, 'rb')
            a = numpy.fromfile(f, dtype=numpy.uint64)
            self.reqTimes = a.reshape((a.shape[0]//1, 1))[int(skip):,:]
            f.close()
        else:
            # print("looking for: ", fileName)
            self.reqTimes = numpy.empty([0,0])
            for bidx in range(1,binRange+1):
                for cidx in range(1,cRange+1):
                    fileName = join(filePath,str(qps))+"/sendTimes-"+str(bidx)+".bin"
                    # print("looking for: ", fileName)
                    # fileName = join(filePath,'lats.bin')
                    if os.path.exists(fileName):
                        f = open(fileName, 'rb')
                        a = numpy.fromfile(f, dtype=numpy.uint64) # a will be 1-D array of all data
                        # self.reqTimes = numpy.append(self.reqTimes, a[:], axis=None)
                        single = a.reshape((a.shape[0]//1, 1))[int(skip):,:] # trasnsfer to 2-D array, apply skip if needed
                        self.reqTimes = numpy.append(self.reqTimes, single.reshape((1,single.shape[0]*1)), axis=None)
                        f.close()
                    else:
                        print("not found in this dir :", fileName)
            self.reqTimes = self.reqTimes.reshape((self.reqTimes.shape[0]//1, 1))
