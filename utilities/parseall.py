#!/usr/bin/python3
import warnings
import sys
import os
import numpy as np
from scipy import stats
import re

warnings.simplefilter(action='ignore', category=FutureWarning)

fileName = sys.argv[1]
f = open(fileName, 'rb')
# f = open('/research/hzhuo2/tailbench/img-dnn/testout.txt','rb')
res = np.loadtxt(f)
ave = np.average(res, axis=0)
std = np.std(res, axis=0)
print ("mean latency %.3f ms | 95th percentile latency %.3f ms | 99th latency %.3f ms | max latency %.3f ms" \
                % (ave[0], ave[1], ave[2], ave[3]))
print ("mean     std %.3f ms | 95th percentile     std %.3f ms | 99th     std %.3f ms | max     std %.3f ms" \
                % (std[0], std[1], std[2], std[3]))