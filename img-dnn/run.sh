#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
REQS=100000000 # Set this very high; the harness controls maxreqs

TBENCH_WARMUPREQS=3000 TBENCH_MAXREQS=3000 TBENCH_QPS_ROI=100 TBENCH_QPS_WARMUP=100 TBENCH_MINSLEEPNS=10000 TBENCH_RANDSEED=0 TBENCH_MNIST_DIR=${DATA_ROOT}/img-dnn/mnist ./img-dnn_integrated -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS}
