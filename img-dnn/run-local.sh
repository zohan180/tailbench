#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
REQS=100000000 # Set this very high; the harness controls maxreqs

for QPS in {"100","200","300","400","500","600","700","800","900","1000","1200"}
do
    mkdir -p ../result-imgdnn/${QPS}
    for k in {1..8}
    do
        TBENCH_WARMUPREQS=$(($QPS*10)) TBENCH_MAXREQS=$(($QPS*16)) TBENCH_QPS_ROI=${QPS} TBENCH_QPS_WARMUP=${QPS} TBENCH_MINSLEEPNS=10000 TBENCH_RANDSEED=0 TBENCH_MNIST_DIR=${DATA_ROOT}/img-dnn/mnist ./img-dnn_integrated -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS}

        echo "moving stats to ../result-imgdnn/${QPS}/lats-$k..."
        mv lats.bin ../result-imgdnn/${QPS}/lats-$k.bin
    done
done