#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
REQS=100000000 # Set this very high; the harness controls maxreqs

echo "starting img-dnn ...."

for QPS in {100, 200, 300, 400, 500, 600, 700, 800}
do
	mkdir ../results/imgdnn-1thread/$QPS
	for k in {1..8}
	do
        echo "starting $k run:"
		TBENCH_MAXREQS=$(($QPS*10)) TBENCH_WARMUPREQS=$(($QPS*20)) TBENCH_SERVER=169.254.53.19 \
		     ./img-dnn_server_networked -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS}
	done
done