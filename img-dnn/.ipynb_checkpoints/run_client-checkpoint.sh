#!/bin/bash
mkdir -p ../results/imgdnn-2
for QPS in {"40","80","120","160","200","240","280","320","360","400"}
do
	mkdir -p ../results/imgdnn-2/$QPS
	for k in {1..8}
	do
		sleep 6
		TBENCH_QPS_ROI=$QPS TBENCH_QPS_WARMUP=$QPS TBENCH_SERVER=192.168.1.16 TBENCH_CLIENT_THREADS=1 TBENCH_MNIST_DIR=/home/zohan/tailbench/data/img-dnn/mnist ./img-dnn_client_networked
		echo "moving stats to ./results/imgdnn-2/$QPS/lats-$k..."
		mv lats.bin ../results/imgdnn-2/$QPS/lats-$k.bin
	done
done
