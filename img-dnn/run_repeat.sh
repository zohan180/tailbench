#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
REQS=100000000 # Set this very high; the harness controls maxreqs

REQ_MAX=200
REQ_WARMUP=2000

QPS=200
QPS_WARMUP=200

#increase QPS_WARMUP
for i in {1..5}
do
	for j in {1..5}
	do	
		for k in {1..5}
		do
			echo "$(($QPS)), $(($k*${QPS_WARMUP})), $(($i*${REQ_MAX})), $(($j*${REQ_WARMUP}))" >> testout_200.txt
			#repeat times for averages
			for some in {1..5}
			do
				TBENCH_MAXREQS=$(($j*${REQ_MAX})) TBENCH_WARMUPREQS=$(($i*${REQ_WARMUP})) TBENCH_SERVER=localhost \
					./img-dnn_server_networked -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS} &
				echo $! > server.pid

				sleep 5 # Wait for server to come up

				TBENCH_QPS_ROI=${QPS} TBENCH_QPS_WARMUP=$(($k*${QPS_WARMUP})) TBENCH_MNIST_DIR=${DATA_ROOT}/img-dnn/mnist ./img-dnn_client_networked &

				echo $! > client.pid
				wait $(cat client.pid)
				# Clean up
				./kill_networked.sh
				../utilities/parselats.py lats.bin >> latsout.txt
			done
			../utilities/parseall.py latsout.txt >> testout_200.txt
			rm latsout.txt
		done
	done
done

REQ_MAX=300
REQ_WARMUP=3000

QPS=300
QPS_WARMUP=300
for i in {1..5}
do
	for j in {1..5}
	do	
		for k in {1..5}
		do
			echo "$(($QPS)), $(($k*${QPS_WARMUP})), $(($i*${REQ_MAX})), $(($j*${REQ_WARMUP}))" >> testout_300.txt
			#repeat times for averages
			for some in {1..5}
			do
				TBENCH_MAXREQS=$(($j*${REQ_MAX})) TBENCH_WARMUPREQS=$(($i*${REQ_WARMUP})) TBENCH_SERVER=localhost \
					./img-dnn_server_networked -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS} &
				echo $! > server.pid

				sleep 5 # Wait for server to come up

				TBENCH_QPS_ROI=${QPS} TBENCH_QPS_WARMUP=$(($k*${QPS_WARMUP})) TBENCH_MNIST_DIR=${DATA_ROOT}/img-dnn/mnist ./img-dnn_client_networked &

				echo $! > client.pid
				wait $(cat client.pid)
				# Clean up
				./kill_networked.sh
				../utilities/parselats.py lats.bin >> latsout.txt
			done
			../utilities/parseall.py latsout.txt >> testout_300.txt
			rm latsout.txt
		done
	done
done


REQ_MAX=400
REQ_WARMUP=4000

QPS=400
QPS_WARMUP=400
for i in {1..5}
do
	for j in {1..5}
	do	
		for k in {1..5}
		do
			echo "$(($QPS)), $(($k*${QPS_WARMUP})), $(($i*${REQ_MAX})), $(($j*${REQ_WARMUP}))" >> testout_400.txt
			#repeat times for averages
			for some in {1..5}
			do
				TBENCH_MAXREQS=$(($j*${REQ_MAX})) TBENCH_WARMUPREQS=$(($i*${REQ_WARMUP})) TBENCH_SERVER=localhost \
					./img-dnn_server_networked -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS} &
				echo $! > server.pid

				sleep 5 # Wait for server to come up

				TBENCH_QPS_ROI=${QPS} TBENCH_QPS_WARMUP=$(($k*${QPS_WARMUP})) TBENCH_MNIST_DIR=${DATA_ROOT}/img-dnn/mnist ./img-dnn_client_networked &

				echo $! > client.pid
				wait $(cat client.pid)
				# Clean up
				./kill_networked.sh
				../utilities/parselats.py lats.bin >> latsout.txt
			done
			../utilities/parseall.py latsout.txt >> testout_400.txt
			rm latsout.txt
		done
	done
done


REQ_MAX=500
REQ_WARMUP=5000

QPS=500
QPS_WARMUP=500
for i in {1..5}
do
	for j in {1..5}
	do	
		for k in {1..5}
		do
			echo "$(($QPS)), $(($k*${QPS_WARMUP})), $(($i*${REQ_MAX})), $(($j*${REQ_WARMUP}))" >> testout_500.txt
			#repeat times for averages
			for some in {1..5}
			do
				TBENCH_MAXREQS=$(($j*${REQ_MAX})) TBENCH_WARMUPREQS=$(($i*${REQ_WARMUP})) TBENCH_SERVER=localhost \
					./img-dnn_server_networked -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS} &
				echo $! > server.pid

				sleep 5 # Wait for server to come up

				TBENCH_QPS_ROI=${QPS} TBENCH_QPS_WARMUP=$(($k*${QPS_WARMUP})) TBENCH_MNIST_DIR=${DATA_ROOT}/img-dnn/mnist ./img-dnn_client_networked &

				echo $! > client.pid
				wait $(cat client.pid)
				# Clean up
				./kill_networked.sh
				../utilities/parselats.py lats.bin >> latsout.txt
			done
			../utilities/parseall.py latsout.txt >> testout_500.txt
			rm latsout.txt
		done
	done
done