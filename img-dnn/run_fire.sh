#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
REQS=100000000 # Set this very high; the harness controls maxreqs

echo "starting img-dnn ...."

echo "$(date) $(ls -1 | wc -l)" > /root/testout_imgdnn.txt

# cd tailbench/img-dnn

for i in {1..5}
do
    TBENCH_WARMUPREQS=500 TBENCH_MAXREQS=1000 TBENCH_SERVER=localhost \
	./img-dnn_server_networked -r ${THREADS} -f ${DATA_ROOT}/img-dnn/models/model.xml -n ${REQS} &
	echo $! > server.pid

	sleep 5 # Wait for server to come up

	TBENCH_QPS=$(($i*50)) TBENCH_MNIST_DIR=${DATA_ROOT}/img-dnn/mnist ./img-dnn_client_networked &

	echo $! > client.pid	
	wait $(cat client.pid)

	# Clean up
	echo "call clean up\n"
	./kill_networked.sh

	echo "$(($i*100)): $(date) $(ls -1 | wc -l)"  >> /root/testout_imgdnn.txt
	../utilities/parselats.py lats.bin >> /root/testout_imgdnn.txt
done

echo "finishing img-dnn"
