#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

AUDIO_SAMPLES='audio_samples'

echo "starting sphinx ...."

for QPS in {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0}
do
    mkdir -p ../results/sphinx-2-mix/$QPS
	echo "starting $thread threads:"
	for k in {1..8}
	do
		sleep 10
		TBENCH_QPS_ROI=$QPS TBENCH_QPS_WARMUP=$QPS TBENCH_SERVER=192.168.1.10 TBENCH_RANDSEED=3496 TBENCH_MINSLEEPNS=10000 TBENCH_AN4_CORPUS=/home/zohan/tailbench/data/sphinx TBENCH_AUDIO_SAMPLES=${AUDIO_SAMPLES} ./decoder_client_networked
		echo "moving stats to ./results/sphinx-2-mix/$QPS/lats-$k..."
		mv lats.bin ../results/sphinx-2-mix/$QPS/lats-$k.bin
	done
done

echo "finishing sphinx"
