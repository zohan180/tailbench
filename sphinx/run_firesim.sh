#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
AUDIO_SAMPLES='audio_samples'

echo "starting sphinx ...."
echo "$(date) $(ls -1 | wc -l)" > /root/testout_sphinx.txt

# for ((k=400;k<542;k+=2))
# do
#     i=$(bc <<<"scale=2; $k / 100" )    # when k=402 you get i=4.02, etc.


for k in {1..5}
do

	i=$(bc <<<"scale=1; $k * 2 / 10" )    # when k=402 you get i=4.02, etc.
	echo "starting run[$(($k))] ...."

	LD_LIBRARY_PATH=./sphinx-install/lib:${LD_LIBRARY_PATH} \
	    TBENCH_MAXREQS=1 TBENCH_WARMUPREQS=1 \
	    ./decoder_server_networked -t $THREADS &

	echo $! > server.pid

	sleep 2

	TBENCH_QPS=$(bc <<<"scale=1; $k *2/ 10" ) TBENCH_MINSLEEPNS=10000 TBENCH_AN4_CORPUS=${DATA_ROOT}/sphinx \
	    TBENCH_AUDIO_SAMPLES=${AUDIO_SAMPLES} ./decoder_client_networked &

	echo $! > client.pid

	wait $(cat client.pid)

	# Cleanup
	./kill_networked.sh
	rm server.pid client.pid 

	echo "$(($TBENCH_QPS))"  >> /root/testout_sphinx.txt
    ../utilities/parselats.py lats.bin >> /root/testout_sphinx.txt
    echo "finihsed at: $(date) $(ls -1 | wc -l)" >> /root/testout_sphinx.txt
done

echo "finishing sphinx"
