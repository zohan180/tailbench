#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
AUDIO_SAMPLES='audio_samples'

LD_LIBRARY_PATH=./sphinx-install/lib:${LD_LIBRARY_PATH} \
    TBENCH_MAXREQS=10 TBENCH_WARMUPREQS=2 \
    ./decoder_server_networked -t $THREADS &

echo $! > server.pid

sleep 5

LD_LIBRARY_PATH=./sphinx-install/lib:${LD_LIBRARY_PATH} \
    TBENCH_QPS_ROI=1 TBENCH_QPS_WARMUP=1 TBENCH_MINSLEEPNS=10000 TBENCH_AN4_CORPUS=${DATA_ROOT}/sphinx \
    TBENCH_AUDIO_SAMPLES=${AUDIO_SAMPLES} ./decoder_client_networked &

echo $! > client.pid

wait $(cat client.pid)

echo "$(date) $(ls -1 | wc -l)" > testout_sphinx.txt
../utilities/parselats.py lats.bin >> testout_sphinx.txt

# Cleanup
./kill_networked.sh
rm server.pid client.pid 
