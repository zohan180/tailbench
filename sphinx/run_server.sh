#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/../configs.sh

THREADS=1
AUDIO_SAMPLES='audio_samples'
echo "starting sphinx ...."

for QPS in {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0}
do
    mkdir -p ../results/sphinx-1/$QPS
	echo "starting $thread threads:"
	for k in {1..2}
	do
		LD_LIBRARY_PATH=./sphinx-install/lib:${LD_LIBRARY_PATH} TBENCH_SERVER=172.16.0.2 \
	    TBENCH_MAXREQS=$(($QPS*10)) TBENCH_WARMUPREQS=$(($QPS*20)) ./decoder_server_networked -t $THREADS 
	done
done

echo "finishing sphinx"
